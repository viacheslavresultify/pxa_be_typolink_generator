<?php

namespace Pixelant\PxaBeTypolinkGenerator\ViewHelpers\Uri;

use Pixelant\PxaBeTypolinkGenerator\Utility\TypoLinkUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Class TypoLinkViewHelper
 * @package Pixelant\PxaBeTypolinkGenerator\ViewHelpers\Uri
 */
class TypolinkViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    /**
     * Register arguments
     */
    public function initializeArguments()
    {
        $this->registerArgument('pageUid', 'int', 'Page UID', true);
        $this->registerArgument('additionalParameters', 'array', 'Additional link paramenters', false, []);
    }

    /**
     * Get link
     *
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $pageUid = (int)$arguments['pageUid'];

        return TypoLinkUtility::getLinkUri($pageUid, $arguments['additionalParameters']);
    }
}
