# BE typolink generator #

Provide ViewHelpers to generate FE link within BE environment.

**Usage**

- Add view helper namespace

`{namespace pxa=Pixelant\PxaBeTypolinkGenerator\ViewHelpers}`

- Use link view helper

`<pxa:typolink pageUid="1" additionalParameters="{tx_myext_pi1: {record: 1}}">Link</pxa:typolink>`

- Use uri view helper

`<a href="{pxa:uri.typolink(pageUid: 1, additionalParameters: '{tx_myext_pi1: {record: 1}}')}">Link</a>`

## Responsible developer ##
@Andriy_Oprysko